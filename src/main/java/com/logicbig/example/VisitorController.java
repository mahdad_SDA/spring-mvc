package com.logicbig.example;

import com.logicbig.example.model.Customer;
import com.logicbig.example.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import java.util.ArrayList;
import java.util.List;

@Controller
public class VisitorController {

    private final CustomerRepository customerRepository;
    private List<String> visitors = new ArrayList<>();

    @Autowired
    public VisitorController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @PostMapping("/visitor")
    public String handlePostRequest(String visitorName) {
        Customer customer = new Customer(visitorName);
        customerRepository.save(customer);
//        visitors.add(visitorName);
        return "redirect:/visitor";
    }


    @GetMapping("/visitor")
    public String handleGetRequest(Model model) {
        model.addAttribute("customers", customerRepository.findAll());
        return "visitor-view";
    }

}