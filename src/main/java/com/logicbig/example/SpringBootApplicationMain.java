package com.logicbig.example;

import com.logicbig.example.model.Customer;
import com.logicbig.example.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApplicationMain implements CommandLineRunner {


    private final CustomerRepository customerRepository;

    @Autowired
    public SpringBootApplicationMain(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApplicationMain.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Customer customer =  new Customer("Oleg");
        System.out.println(customerRepository.save(customer));
    }
}