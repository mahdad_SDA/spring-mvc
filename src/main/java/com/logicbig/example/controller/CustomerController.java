package com.logicbig.example.controller;

import com.logicbig.example.model.Customer;
import com.logicbig.example.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomerController {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @PostMapping("/customer")
    public String handlePostRequest(String name, String surname) {
        Customer customer = new Customer(name);
        customer.setSurname(surname);
        customerRepository.save(customer);
//        visitors.add(visitorName);
        return "redirect:/customer";
    }

    @GetMapping("/customer")
    public String handleGetRequest(Model model) {
        model.addAttribute("customers", customerRepository.findAll());
        return "customer-view";
    }

}
